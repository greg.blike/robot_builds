FROM ros:noetic
SHELL ["/bin/bash", "-c"]

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN rosdep update

RUN apt-get install -y \
  git \
  ros-${ROS_DISTRO}-rtabmap-ros\
  ros-${ROS_DISTRO}-realsense2-camera\
  ros-${ROS_DISTRO}-arbotix\
  ros-${ROS_DISTRO}-rgbd-launch\
  ros-${ROS_DISTRO}-libuvc-ros\
  ros-${ROS_DISTRO}-libuvc-camera\
  ros-${ROS_DISTRO}-audio-common\
  ros-${ROS_DISTRO}-navigation\
  ros-${ROS_DISTRO}-moveit\
  ros-${ROS_DISTRO}-rviz-visual-tools\
  ros-${ROS_DISTRO}-moveit-visual-tools\
  ros-${ROS_DISTRO}-desktop-full\
  ros-${ROS_DISTRO}-catkin\
  libgoogle-glog-dev\
  python3-rosdep\
  python3-rosinstall\
  python3-rosinstall-generator\
  python3-catkin-tools\
  python3-osrf-pycommon\
  python3-wstool\
  build-essential

RUN mkdir -p /catkin_ws/src

WORKDIR /catkin_ws/src
RUN git clone https://github.com/ros-planning/moveit_grasps.git
RUN git clone https://github.com/ros-planning/moveit_tutorials.git -b master
RUN git clone https://github.com/ros-planning/panda_moveit_config.git -b ${ROS_DISTRO}-devel
RUN git clone https://github.com/orbbec/ros_astra_camera
RUN git clone https://github.com/ros-planning/moveit_task_constructor.git
RUN sed -i '69s/rgb/color/' /catkin_ws/src/moveit_tutorials/doc/perception_pipeline/src/detect_and_add_cylinder_collision_object_demo.cpp

WORKDIR /catkin_ws
RUN source /opt/ros/${ROS_DISTRO}/setup.bash && rosdep install -y --from-paths . --ignore-src --rosdistro ${ROS_DISTRO}
RUN source /opt/ros/${ROS_DISTRO}/setup.bash && catkin build

RUN echo "source /catkin_ws/devel/setup.bash" >> ${HOME}/.bashrc
