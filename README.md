Run this to install docker

```
#!/bin/sh

sudo apt-get update
sudo apt-get install ca-certificates curl gnupg

sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

```
xhost +local:root
docker run -it --rm   --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" speedy bash
```

More commands to run

```
sudo usermod -a -G docker $USER
sudo apt-get -y install git



git clone https://gitlab.com/greg.blike/robot_builds.git $HOME/robot_builds

# For shared desktop
sudo apt-get -y install vino
gsettings set org.gnome.Vino require-encryption false

#remote shell login
sudo apt-get install -y openssh-server
sudo systemctl enable ssh.service
sudo systemctl start ssh.service

mkdir -p $HOME/.local/bin
ln -s $HOME/robot_builds/mkdockerrosenv $HOME/.local/bin/
ln -s $HOME/robot_builds/rosattach $HOME/.local/bin/

echo 'export PATH=$PATH:$HOME/.local/bin' >> $HOME/.bashrc
source $HOME/.bashrc

```

```
sudo apt-get upgrade
sudo reboot
```

```
cd ~/robot_builds
```

```
# Make the image for ROS
docker build -t speedy .

# Pull out the workspace that was built
# Use <CONTAINER_NAME>_catkin_ws
docker_extract speedy ~/rosmain_catkin_ws

# Make a container that mounts ~/<CONTAINER_NAME>_catkin_ws
mkdockerrosenv rosmain speedy

# Start the container
docker start rosmain

# Attach to the running container
rosattach rosmain
```
