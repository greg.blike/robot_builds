FROM speedy

ENV DEBIAN_FRONTEND=noninteractive

# Install opencv
# RUN apt purge libopencv-dev libopencv*
RUN apt update && apt install -y \
	build-essential \
	cmake \
	git \
	libgtk2.0-dev \
	pkg-config \
	libavcodec-dev \
	libavformat-dev \
	libswscale-dev \
	python-dev \
	python3-dev \
	python-numpy \
	libtbb2 \
	libtbb-dev \
	libjpeg-dev \
	libpng-dev \
	libtiff-dev \
	libdc1394-22-dev \
	python3-pip \
	python3-numpy
	#ubuntu-restricted-extras \
	#gstreamer1.0* \
	#libgstreamer1.0-dev \
	#libgstreamer-plugins-base1.0-dev

WORKDIR /root
ENV VERSION=4.2.0
RUN git clone https://github.com/opencv/opencv_contrib.git -b ${VERSION} --depth 1
RUN git clone https://github.com/opencv/opencv.git -b ${VERSION} --depth 1

RUN mkdir -p /root/opencv/build
WORKDIR /root/opencv/build

RUN cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/opt/opencv4 \
	-D OPENCV_GENERATE_PKGCONFIG=ON -D BUILD_EXAMPLES=OFF \
	-D INSTALL_PYTHON_EXAMPLES=OFF -D INSTALL_C_EXAMPLES=OFF \
	-D PYTHON_EXECUTABLE=$(which python2) -D BUILD_opencv_python2=OFF \
	-D PYTHON3_EXECUTABLE=$(which python3) \
	-D PYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
	-D PYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
	-D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules/ \
	-D WITH_GSTREAMER=ON ..
RUN make && make install
ENV CMAKE_PREFIX_PATH=/opt/opencv4:$CMAKE_PREFIX_PATH
ENV LD_LIBRARY_PATH=/opt/opencv4/lib:$LD_LIBRARY_PATH

WORKDIR /root
RUN git clone -b 2021.4.2 --depth 1 https://github.com/openvinotoolkit/openvino.git 
WORKDIR /root/openvino
RUN git submodule update --init --recursive
RUN chmod +x install_build_dependencies.sh
RUN mkdir build
WORKDIR /root/openvino/build
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make -j8
RUN make install

WORKDIR /root
RUN git clone https://github.com/IntelRealSense/librealsense.git
RUN mkdir -p /root/librealsense/build
WORKDIR /root/librealsense/build
RUN cmake ../
RUN make && make install

WORKDIR /catkin_ws

# install openvino 2021.4
# https://docs.openvinotoolkit.org/latest/openvino_docs_install_guides_installing_openvino_apt.html
#RUN apt update && apt install --assume-yes curl gnupg2 lsb-release
#RUN curl -s https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB |apt-key add -
#RUN echo "deb https://apt.repos.intel.com/openvino/2021 all main" | tee /etc/apt/sources.list.d/intel-openvino-2021.list
#RUN apt update
#RUN apt-cache search openvino
#RUN apt-get install -y intel-openvino-dev-ubuntu20-2021.4.582 
#RUN ls -lh /opt/intel/openvino_2021
#RUN source /opt/intel/openvino_2021/bin/setupvars.sh 


# install librealsense2
#RUN apt-get install -y --no-install-recommends \
#software-properties-common 
# https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md
#RUN apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE 
#RUN add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo bionic main" -u \
#&& apt-get install -y --no-install-recommends \
#librealsense2-dkms \
#librealsense2-utils \
#librealsense2-dev \
#librealsense2-dbg \
#libgflags-dev \
#libboost-all-dev \
#&& rm -rf /var/lib/apt/lists/*

RUN echo "source /usr/local/bin/setupvars.sh" >> ${HOME}/.bashrc
