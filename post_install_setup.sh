#!/bin/bash

sudo apt-get -y install git

git clone https://gitlab.com/greg.blike/robot_builds.git $HOME/robot_builds

# For shared desktop
sudo apt-get -y install vino
gsettings set org.gnome.Vino require-encryption false

#remote shell login
sudo apt-get install -y openssh-server
sudo systemctl enable ssh.service
sudo systemctl start ssh.service

mkdir -p $HOME/.local/bin
ln -s $HOME/robot_builds/mkdockerrosenv $HOME/.local/bin/
ln -s $HOME/robot_builds/rosattach $HOME/.local/bin/

echo 'export PATH=$PATH:$HOME/.local/bin' >> $HOME/.bashrc
source $HOME/.bashrc
