```bash
docker build -t xarm .
mkdir -p ~/xarm_dev_catkin_ws/src
git clone https://github.com/gblike/xArm_Lewansoul_ROS.git -b gripper ~/xarm_dev_catkin_ws/src/xArm_Lewansoul_ROS
mkdockerrosenv xarm_dev xarm
docker start xarm_dev
rosattach xarm_dev
catkin build
source devel/setup.bash
roslaunch xarm_moveit_config demo.launch
```
